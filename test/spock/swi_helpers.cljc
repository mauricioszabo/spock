(ns spock.swi-helpers
  #?(:cljs (:require-macros [spock.swi-helpers]))
  (:require [check.async :as a]))

#?(:cljs (defonce runtime (atom nil)))

(defmacro with-prolog [ rules & body]
  (let [is-cljs? (:ns &env)]
    (if is-cljs?
      `(let [^js ~'prolog (spock/with-rules @runtime ~rules)]
         (try
           ~@body
           (finally (.dispose ~'prolog))))
      `(with-open [~'prolog (spock/with-rules ~rules)]
         ~@body))))

(defmacro with-assert-prolog [ rules & body]
  (let [is-cljs? (:ns &env)]
    (if is-cljs?
      `(let [^js ~'prolog (spock/assert-rules @runtime ~rules)]
         (try
           ~@body
           (finally (.dispose ~'prolog))))
      `(with-open [~'prolog (spock/assert-rules ~rules)]
         ~@body))))
