(ns spock.swi-test
  (:require [clojure.test :refer [deftest testing]]
            [spock.swi-helpers :as h]
            [check.core :refer [check]]
            [spock.swi :as spock]
            [matcher-combinators.matchers :as m]
            #?(:cljs ["fs" :as fs])))

(def family-rules
  '[(parent [father son])
    (parent [son grandson])

    (ancestor [:x :y] [(parent :x :y)])
    (ancestor [:x :y] [(parent :x :z)
                       (ancestor :z :y)])])

(defn solve
  ([query] (solve {} query))
  ([opts query]
   (spock/solve #?(:cljs (assoc opts :runtime @h/runtime) :clj opts)
                query)))

(deftest simple-solver
  (h/with-prolog family-rules
    (testing "will unify simple rules"
      (check (solve '(= 1 1)) => [{}])
      (check (solve '(= 2 1)) => [])
      (check (solve '(= :x "A")) => [{:x #?(:clj 'A :cljs "A")}])
      #?(:cljs (check (solve '(= :x "á")) => [{:x "á"}]))
      #?(:cljs (check (solve '(= :x [1 2 3 4 5 6 7 8 9 10 11]))
                      => [{:x [1 2 3 4 5 6 7 8 9 10 11]}]))
      (check (solve '(= :x-a true)) => [{:x-a true}])
      (check (solve '(= :x false)) => [{:x false}]))

    (testing "will make queries and bound unbound variables"
      (check (solve {:rules prolog} '(ancestor father :child))
             => (m/in-any-order '[{:child son}
                                  {:child grandson}])))

    (testing "will make queries and allow to seed unbound vars"
      (check (solve {:rules prolog, :bind {:x 'father}} '(ancestor :x :child))
             => (m/in-any-order '[{:child son}
                                  {:child grandson}])))

    (testing "will unify simple rules with multiple vars"
      (let [underscore #(and (keyword? %) (-> % name first str (= "_")))]
        (check (solve '(or (and (= :a 1) (= :b 1))
                           (and (= :a 2) (= :b 2))
                           (and (= :a :b))))
               => (m/in-any-order [{:a 1 :b 1}
                                   {:a 2 :b 2}
                                   {:a underscore :b underscore}]))))))

#?(:cljs
   (deftest ignore-underscore-vars
     "Because we need to use some internal vars, they should not appear on our result"
     (check (solve '(and (= :a 10) (= :_b 20))) => [(m/equals {:a 10})])))

(deftest conversions
  (testing "will rename only rules' symbols"
    #?(:cljs (spock/use-library @h/runtime "lists"
                                (str (fs/readFileSync "wasm/wasm-preload/library/lists.pl"))))
    (h/with-prolog '[(get-odd [:a] [(member :a [1 3 5])])]
      (check (solve {:rules prolog} '(get-odd :odd))
             => (m/in-any-order [{:odd 1}
                                 {:odd 3}
                                 {:odd 5}]))))

  (testing "will convert arrays/numbers"
    (check (solve '(append :x :y [1 2 3]))
           => (m/in-any-order [{:x [] :y [1 2 3]}
                               {:x [1] :y [2 3]}
                               {:x [1 2] :y [3]}
                               {:x [1 2 3] :y []}])))

  (testing "converting some placeholders"
    (check (solve '(= [:_ & :n] [1 2 3 4]))
           => [{:n [2 3 4]}])

    (check (solve '(= [:_ :_ :n] [1 2 3]))
           => [{:n 3}])

    (check (solve '(= 1 1))
           => [{}])

    (check (solve '(not= 1 2))
           => [{}])
    (check (solve '(not= [1 2] [2 1]))
       => [{}])))

(def some-map {"a-key" 10})
(defn- combine-vecs [a b]
  (mapv vector a b))

(defn- prolog-inc [a result]
  (if (keyword? result)
    (if (keyword? a)
      [0 1]
      [a (inc a)])
    [(dec result) result]))

#?(:clj
   (deftest clojure-objects
     (testing "binds CLJ objects to Prolog"
       (check (solve {:bind {:map some-map}} '(jpl_call :map get ["a-key"] :result))
              => [{:result 10}])))

   :cljs
   (deftest clojurescript-functions
     (testing "binds CLJS functions to Prolog"
       (spock/bind-function! @h/runtime :combine-vecs combine-vecs)
       (check (solve '(and
                       (= :fst [1 2])
                       (= :snd [3 4])
                       (combine-vecs :fst :snd :result)))
              => [{:result [[1 3] [2 4]]}]))

     (testing "binds UNIFIERs from ClojureScript to Prolog"
       (spock/bind-unifier! @h/runtime :inc prolog-inc)
       (check (solve '(inc 1 :i)) => [{:i 2}])
       (check (solve '(inc :d 1)) => [{:d 0}])
       (check (solve '(inc :x :i)) => [{:x 0 :i 1}]))

     (testing "captures HUGE outputs"
       (check (solve {:bind {:f (vec (range 400))}}
                     '(combine-vecs :f :f :result))
              => [{:result #(-> % count (= 400))}]))))

(deftest ^{:doc "All facts in SWI-Prolog JPL bridge are global. What
with-facts do is rename most of these facts to be local. In this case,
the facts will remain global indeed, and `.close` will retract them."}
  global-asserts

  (h/with-assert-prolog family-rules
    (check (solve '(ancestor father :child))
           => (m/in-any-order '[{:child son}
                                {:child grandson}]))))
