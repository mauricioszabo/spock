(ns spock.all-tests
  (:require [clojure.test :as t]
            [promesa.core :as p]
            [spock.swi-helpers :as helpers]
            [spock.swi-test]
            ["path" :as path]))

(defn- ^:dev/after-load run-tests []
  (t/run-all-tests #"spock.*"))

(defmethod t/report [::t/default :summary] [m]
  (println "\nRan" (:test m) "tests containing"
           (+ (:pass m) (:fail m) (:error m)) "assertions.")
  (println (:pass m) "passes," (:fail m) "failures," (:error m) "errors.")
  (when js/process.env.CI
    (js/process.exit (+ (:fail m) (:error m)))))

(defn main []
  (p/let [swi (js/require "../wasm/swipl-web")
          runtime (spock.swi/create-runtime! swi {:locateFile (fn [file]
                                                                (path/join "wasm" file))})]
    (reset! helpers/runtime runtime)
    (run-tests)))
