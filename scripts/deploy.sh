#!/bin/bash

run_deploy() {
  lein change :profiles:dev:jvm-opts empty
  curl https://raw.githubusercontent.com/mauricioszabo/clj-lib-deployer/master/deploy-lein.bb -o deploy
  curl -L  https://github.com/borkdude/babashka/releases/download/v0.2.5/babashka-0.2.5-linux-amd64.zip -o bb.zip
  unzip bb.zip
  mv bb /usr/bin
  chmod +x deploy
  ./deploy
}

if [ "$CI_COMMIT_TAG" ]; then
  export TAG=$CI_COMMIT_TAG
  run_deploy
elif [ "$CI_COMMIT_REF_NAME" = "master" ]; then
  run_deploy
else
  echo "Skipping deploy"
fi
