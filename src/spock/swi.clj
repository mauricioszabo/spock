(ns spock.swi
  (:require [spock.commons :as commons]
            [clojure.walk :as walk]
            [clojure.set :as set])
  (:import [org.jpl7 Atom Variable Compound Term Query]))

(defn- as-atom [keyword]
  (Atom. (commons/normalize-arg keyword)))

(defn- as-var [symbol]
  (-> symbol name Variable.))

(defprotocol AsProlog
  (to-prolog [this]))

(defn- as-list [this]
  (let [size (count this)]
    (cond
      (and (= 2 size) (-> this first (= '&)))
      (-> this last to-prolog)

      (zero? size)
      (Term/textToTerm "[]")

      :else
      (Compound. "[|]" (into-array Term [(-> this first to-prolog)
                                         (-> this rest as-list)])))))

(defn- as-struct [unparsed]
  (let [[head & tail] (commons/normalize-struct unparsed)]
    (Compound. head (into-array Term (map to-prolog tail)))))

(extend-protocol AsProlog
  clojure.lang.Keyword
  (to-prolog [this] (as-var this))

  clojure.lang.Symbol
  (to-prolog [this] (as-atom this))

  clojure.lang.LazySeq
  (to-prolog [this] (as-struct this))
  clojure.lang.Cons
  (to-prolog [this] (as-struct this))
  clojure.lang.PersistentList
  (to-prolog [this] (as-struct this))

  clojure.lang.PersistentVector
  (to-prolog [this] (as-list this))

  Object
  (to-prolog [this]
    (cond
      (string? this) (Term/textToTerm (pr-str this))
      (number? this) (Term/textToTerm (pr-str this))
      :else (org.jpl7.JRef. this))))

(defrecord SWI []
  commons/SWIWrapper
  (call! [_ query] (-> query as-struct Query. .oneSolution)))

(defn with-rules [rules]
  (commons/with-rules (->SWI) rules))

(defn assert-rules [rules]
  (commons/assert-rules (->SWI) rules))

(defprotocol FromProlog
  (from-prolog [this]))

(defn- from-compound [^Term term]
  (if (.isList term)
    (->> term .listToTermArray (mapv from-prolog))
    (let [name (.name term)
          fields (->> term .args (map from-prolog))]
      (doall (cons (symbol name) fields)))))

(extend-protocol FromProlog
  Atom
  (from-prolog [this]
    (if (.isList this)
      (from-compound this)
      (-> this .name symbol)))

  Variable
  (from-prolog [this] (-> this .name keyword))

  org.jpl7.Integer
  (from-prolog [this]
    (if (.isBigInteger this)
      (-> this str bigint)
      (.longValue this)))

  org.jpl7.Float
  (from-prolog [this] (.doubleValue this))

  Compound
  (from-prolog [this] (from-compound this))

  org.jpl7.JRef
  (from-prolog [this] (.object this)))

(defn solve
  ([query] (solve {} query))
  ([opts query]
   (let [[query from-prolog] (commons/prepare-solve from-prolog opts query)]
     (->> query
          to-prolog
          Query.
          iterator-seq
          (mapv (fn [match]
                  (->> (.entrySet match)
                       (map (fn [e] [(-> e .getKey keyword)
                                     (-> e .getValue from-prolog)]))
                       (into {}))))))))
