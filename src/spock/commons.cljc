(ns spock.commons
  (:require [clojure.walk :as walk]
            [clojure.walk :as walk]
            [clojure.set :as set]))

(defn- compose-with [tail between wrap]
  (let [tail (if wrap
               (->> tail
                    (partition 2 1)
                    (map (partial apply list wrap)))
               tail)]
    ((fn rec [[f & rs]]
       (if (seq rs)
         (list between f (rec rs))
         f))
     tail)))

(defn- inner-normalize-struct [struct-list]
  (let [[tag & body] struct-list]
    (case tag
      not= (compose-with body "," "\\=")
      = (compose-with body "," "=")
      or (compose-with body ";" nil)
      and (compose-with body "," nil)
      :- (list ":-"
               (first body)
               (compose-with (rest body) "," nil))
      (apply list (name tag) body))))

(defn normalize-struct [struct-list]
  (walk/postwalk #(cond-> % (seq? %) inner-normalize-struct)
                struct-list))

(defn normalize-arg [nameable]
   (case nameable
     not= "\\="
     or ";"
     and ","
     :- ":-"
     (name nameable)))

(def compound-types
  #{"=\\="
    "="
    "-"
    "+"
    "*"
    "/"
    ","
    ";"
    ":-"})

(defprotocol SWIWrapper
  (call! [_ query]))
  ; (multiple-results! [_ query]))

(defprotocol ITemporaryFacts
  (rename-query [_ q])
  (rename-result [_ res]))

(defn- retract-all [swi rules]
  (doseq [rule rules
          :let [retraction (->> rule rest (cons 'retract))]]
    (call! swi retraction)))

(defrecord TemporaryFacts [swi rules renames]
  ITemporaryFacts
  (rename-query [_ q]
    (walk/postwalk-replace renames q))

  (rename-result [_ res]
    (walk/postwalk-replace (set/map-invert renames) res))

  #?@(:clj
      [java.io.Closeable
       (close [_] (retract-all swi rules))]

      :cljs
      [Object
       (dispose [_] (retract-all swi rules))]))

(defn- as-fact [fact-name params]
  (->> params
       (cons fact-name)
       (list 'assert)))

(defn- as-rule [fact-name params rules]
  (let [body (cons 'and rules)
        fact (->> params
                  (cons fact-name))]
    (->> body
         (list :- fact)
         (list 'assert))))

(defn- normalize-rules [rules]
  (for [row rules
        :let [parsed (case (count row)
                       1 (list 'assert row)
                       2 (as-fact (nth row 0) (nth row 1))
                       3 (as-rule (nth row 0) (nth row 1) (nth row 2)))]]
    parsed))

(defn with-rules [swi rules]
  (let [trs (->> rules
                 (map first)
                 (map (juxt identity #(gensym (str % "-"))))
                 (into {}))
        randomize (memoize #(cond-> % (symbol? %) (trs %)))
        new-rules (walk/postwalk randomize rules)
        rules (normalize-rules new-rules)]
    (doseq [rule rules] (call! swi rule))
    (->TemporaryFacts swi rules trs)))

(defn assert-rules [swi rules]
  (let [rules (normalize-rules rules)]
    (doseq [rule rules] (call! swi rule))
    (->TemporaryFacts swi rules {})))

(defn- bind-vals [bind query]
  (let [binds (mapv (fn [[k v]] (list '= k v)) bind)
        and-query (conj binds query)]
    (cons 'and and-query)))

(defn prepare-solve [from-prolog {:keys [rules bind]} query]
  (let [query (cond->> query
                       bind (bind-vals bind)
                       rules (rename-query rules))
        from-prolog (if rules
                      (comp #(rename-result rules %) from-prolog)
                      from-prolog)]
    [query from-prolog]))
