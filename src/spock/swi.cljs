(ns spock.swi
  (:require [instaparse.core :as insta]
            [spock.commons :as commons]
            [clojure.walk :as walk]
            [clojure.string :as str]))

(declare parse-terms)
(defn- parse-all-terms [text acc closing]
  (if (= (aget text 0) closing)
    [acc (subs text 1)]
    (let [[term rest] (parse-terms text)
          acc (conj acc term)
          pref (subs rest 0 2)]
      (cond
        (= ", " pref) (recur (subs rest 2) acc closing)
        (= "," (aget pref 0)) (recur (subs rest 1) acc closing)
        :else (recur rest acc closing)))))

(defn- norm-atom [var-name]
  (-> var-name
      str/lower-case
      (str/replace-first #"^'" "")
      (str/replace-first #"'$" "")
      (str/replace #"_" "-")
      symbol))

(defn- parse-as-atom [text]
  (let [t (re-find #"[\d\w_]*" text)
        rest (subs text (count t))
        atom (norm-atom t)]
    (if (-> rest (aget 0) (= "("))
      (let [[terms rest] (parse-all-terms (subs rest 1) [] ")")]
        [(cons atom terms) rest])
      [atom rest])))

(defn- parse-terms [text]
  (case (aget text 0)
    "f" (if (-> text (subs 0 5) (= "false"))
          [false (subs text 5)]
          (parse-as-atom text))
    "t" (if (-> text (subs 0 4) (= "true"))
          [true (subs text 4)]
          (parse-as-atom text))
    "[" (let [[terms rest] (parse-all-terms (subs text 1) [] "]")]
          [terms rest])
    "'" (let [atom (re-find #"'(?:\\'|[^']*)'" text)
              s (count atom)
              rest (subs text s)
              atom (norm-atom (subs atom 1 (dec s)))]
          (if (-> rest (aget 0) (= "("))
            (let [[terms rest] (parse-all-terms (subs rest 1) [] ")")]
              [(cons atom terms) rest])
            [atom rest]))
    "\"" (let [atom (re-find #"\"(?:\\\"|[^\"])+?\"" text)
               s (count atom)]
           [(subs atom 1 (dec s)) (subs text s)])
    ("1" "2" "3" "4" "5" "6" "7" "8" "9" "0") (let [t (re-find #"\d+(?:\.\d+)?" text)]
                                                [(js/Number t) (subs text (count t))])

    ;; ELSE
    (cond
      (re-find #"^[A-Z_]" text) (let [t (re-find #"[\d\w_]+" text)]
                                  (-> t
                                      str/lower-case
                                      (str/replace #"_" "-")
                                      keyword
                                      (vector (subs text (count t)))))
      (re-find #"^[a-z]" text) (parse-as-atom text))))

(defn parse-full-term [text]
  (let [[terms r] (parse-terms text)]
    (if (seq r)
      (throw (ex-info "Leading chars" {:chars r}))
      terms)))

(defn- parse-root [text]
  (case text
    "false" false
    "true" true
    (let [[a b] (str/split text " = ")]
      (ex-info "Leading chars" {:chars r})
      (-> a
          str/lower-case
          (str/replace #"_" "-")
          keyword
          (vector (parse-full-term b))))))

(def decoder (js/TextDecoder.))
(defn- normalize-stdout [out]
  (let [array (js/Uint8Array.from out)]
    (-> (.decode decoder array)
        (str/replace #"(\n|\s)+" " ")
        str/trim
        (str/replace #"\.$" ""))))

(def ^:private encoder (js/TextEncoder.))
(defn stdin-code [streams]
  (when-not (:error @streams)
    (try
      (let [{:keys [pos code out next?]} @streams]
        (if (< pos (.-length code))
          (let [code (aget code pos)]
            (swap! streams update :pos inc)
            code)
          (when next?
            (swap! streams
                   (fn [streams]
                     (-> streams
                         (assoc :code (. encoder encode "n")
                                :pos 0
                                :out #js []
                                :next? false)
                         (update :parsed conj (normalize-stdout (:out streams))))))
            (stdin-code streams))))
      (catch :default e
        (prn :ERROR :IN e)))))

(declare to-prolog)
(defn stdout-code [streams char]
  (when-not (:error @streams)
    (let [out (:out @streams)
          as-str (js/String.fromCharCode char)]
      (.push out char)
      (swap! streams assoc :next? (and (not= as-str ".")
                                       (not= as-str "\n"))))))

(defn prepare-run [streams ^js module]
  (.. module -FS (init (partial stdin-code streams)
                       (partial stdout-code streams))))


(defn- run-fn [{:keys [kind impl]} params]
  (case kind
    :normal (let [args (pop params)
                  res (apply impl params)]
              (conj args res))
    :unifier (apply impl params)))

(defn- send-result! [streams buffer offset length position]
  (if-let [str-val (-> @streams :to-deliver first)]
    (let [val (.encode encoder str-val)
          size (.-length val)
          send-nth-bytes (fn [size]
                           (dotimes [n size]
                                    (let [buffer-n (+ offset n)]
                                      (aset buffer buffer-n (aget val n))))
                           size)]

      (if (<= size length)
        (do
          (swap! streams update :to-deliver subvec 1)
          (send-nth-bytes size))
        (do
          (swap! streams update-in [:to-deliver 0] subs length)
          (send-nth-bytes length))))
    0))

(defn fake-dev [streams]
  #js {:open (constantly 1)
       :close (constantly nil)
       :read (fn [stream buffer offset length position]
               (send-result! streams buffer offset length position))
       :write (fn [stream buffer offset length position]
                (let [res (.decode decoder (. buffer subarray offset, (+ offset length)))
                      [_ [fn-name & params]] (-> res str/trimr parse-full-term)
                      params (vec params)
                      fun (-> @streams :functions (get fn-name))
                      function-result (try
                                        (-> (run-fn fun params)
                                            to-prolog
                                            (str "."))
                                        (catch :default e
                                          (prn :ERROR e)))]
                  (swap! streams update :to-deliver conj function-result)
                  length))})

(defn create-runtime!
  ([constructor] (create-runtime! constructor {}))
  ([constructor opts]
   (let [streams (atom {:pos 0
                        :code "A = 10."
                        :out #js []
                        :parsed []
                        :to-deliver []
                        :functions {}})]
     (. (constructor (-> {:arguments ["swipl" "-q" "-x" "/src/wasm-preload/boot.prc" "--nosignals"]}
                         (merge opts)
                         (assoc :preRun #js [(partial prepare-run streams)]
                                :printErr #(do
                                             (prn :ERR %)
                                             (swap! streams update :error str % "\n")))
                         clj->js))
       (then (fn [^js r]
               (.. r -FS (mkdirTree "wasm-preload/library"))
               (.. r -FS (mkdirTree "/tmp"))
               (let [id (.. r -FS (makedev 88, 0))]
                 (.. r -FS (registerDevice id (fake-dev streams)))
                 (.. r -FS (mkdev "/dev/cljs-interop" id)))
               (.. r -prolog (call_string (str "set_prolog_flag(answer_write_options,["
                                               "max_depth(0), "
                                               "portray(true), "
                                               "quoted(true), "
                                               "spacing(next_argument)"
                                               "]).")))
               #js {:runtime r
                    :streams streams}))))))

(defn use-library [^js runtime lib-name contents]
  (.. runtime -runtime -FS (writeFile (str "wasm-preload/library/" lib-name ".pl")
                                      contents))
  (.. runtime -runtime -prolog (call_string (str "use_module(library(" lib-name "))"))))

(defn query! [^js swi code]
  (let [streams (.-streams swi)]
    (swap! streams assoc
           :to-deliver []
           :code (. encoder encode code)
           :pos 0
           :out #js []
           :parsed []
           :error nil)
    (.. swi -runtime -prolog (call_string "break"))
    (if-let [error (:error @streams)]
      (throw (ex-info "Query failed!" {:error error}))
      (->> (-> @streams :out normalize-stdout)
           (conj (:parsed @streams))
           (filter identity)))))

(defprotocol AsProlog (to-prolog [this]))

(defn- as-atom [unparsed]
  (str "'" (str/replace (str unparsed) #"\\" "\\\\") "'"))

(defn- as-struct [unparsed]
  (let [[head & tail] (commons/normalize-struct unparsed)]
    (str (as-atom head)
         "("
         (->> tail
              (map to-prolog)
              (str/join ", "))
         ")")))

(extend-protocol AsProlog
  string
  (to-prolog [this] (pr-str this))

  boolean
  (to-prolog [this] (pr-str this))

  number
  (to-prolog [this] (str this))

  Keyword
  (to-prolog [this] (-> this name
                        (str/replace-first #"." (fn [s] (str/upper-case s)))
                        (str/replace #"-" "_")))

  Symbol
  (to-prolog [this] (as-atom this))

  object
  (to-prolog [this]
    (cond
      (vector? this) (let [[bef aft] (split-with #(not= % '&) this)]
                       (str "["
                            (if (-> aft count (= 2))
                              (str (->> bef (map to-prolog) (str/join ", "))
                                   " | " (->> aft last to-prolog))
                              (->> this (map to-prolog) (str/join ", ")))
                            "]"))

      (let [mapped (map to-prolog this)])
      (str "["
           (->> this
                (map to-prolog)
                (str/join ", "))
           "]")
      (list? this) (as-struct this)
      :else (throw (ex-info "Unsupported Object" {:type (type this)})))))

(defrecord SWI [^js runtime]
  commons/SWIWrapper
  (call! [_ query] (.. runtime -runtime -prolog (call_string (to-prolog query)))))

(defn with-rules [runtime rules]
  (commons/with-rules (->SWI runtime) rules))

(defn assert-rules [runtime rules]
  (commons/assert-rules (->SWI runtime) rules))

(defn- get-val-for-key [key vars]
  (let [val (get vars key ::missing)]
    (cond
      (= val ::missing) [::missing key]
      (keyword? val) (get-val-for-key val vars)
      :else [val])))

(defn- unify-vars [vars]
  (if (some keyword? (vals vars))
    (->> vars
         (mapcat (fn [[k v]]
                   (let [[new-val possible-k] (get-val-for-key k vars)]
                     (if (= ::missing new-val)
                       [[k :_] [possible-k :_]]
                       [[k new-val]]))))
         (into {}))
    vars))

(defn- parse-one-result [parse-prolog one-result]
  (let [results (str/split one-result #", (?=[A-Z_].*? =)")]
    (->> results
         (map parse-root)
         (reduce (fn [acc parsed]
                   (cond
                     (vector? parsed) (let [[k v] parsed]
                                        (if (and (keyword? k) (-> k name (str/starts-with? "-")))
                                          acc
                                          (assoc acc k v)))
                     parsed acc))
                 {})
         unify-vars)))

(defn- from-prolog [result-strings]
  (let [parse-prolog (memoize parse-prolog)]
    (try
      (->> result-strings
           (map (fn [one-result]
                  (try
                    (when-not (= "false" one-result)
                      (def one-result one-result))
                    (parse-one-result parse-prolog one-result)
                    (catch :default e
                      (println "Error parsing:" e)
                      (println "Original:" result-strings)
                      :invalid-output))))
           (filter identity))
      (catch :default e
        (println "Error parsing:" e)
        :invalid-output))))

(defn- rewrite-cljs-funs [functions query]
  (walk/postwalk (fn [form]
                   (if (and (list? form) (-> form first functions))
                     (let [w-stream (keyword (str (gensym "_w")))
                           r-stream (keyword (str (gensym "_r")))
                           args-as-is (-> form rest vec)]
                       `(~'and
                          (~'open "/dev/cljs-interop" ~'write ~w-stream)
                          (~'print ~w-stream ~(list 'run-cljs form))
                          (~'close ~w-stream)
                          (~'open "/dev/cljs-interop" ~'read ~r-stream)
                          (~'read ~r-stream ~args-as-is)
                          (~'close ~r-stream)))
                     form))
                 query))

(defn solve [{:keys [runtime] :as opts} query]
  (let [rewroten-query (rewrite-cljs-funs (-> runtime .-streams deref :functions) query)
        [query from-prolog] (commons/prepare-solve from-prolog opts rewroten-query)
        as-str (-> query to-prolog (str "."))]
    (->> as-str
         (query! runtime)
         from-prolog)))

(defn bind-function! [runtime fn-name fn-implementation]
  (swap! (.-streams runtime) assoc-in [:functions (-> fn-name name symbol)]
         {:kind :normal
          :impl fn-implementation})
  nil)

(defn bind-unifier! [runtime fn-name fn-implementation]
  (swap! (.-streams runtime) assoc-in [:functions (-> fn-name name symbol)]
         {:kind :unifier
          :impl fn-implementation})
  nil)
